from django.contrib import admin

# Register your models here.
from . models import StateEntity, Connection

admin.site.register(StateEntity)
admin.site.register(Connection)