from django.db import models

class StateEntity(models.Model):
	entity_name = models.CharField('entity name', max_length = 100)
	entity_description = models.TextField('entity description')

class Connection(models.Model):
	state_entity = models.ForeignKey(StateEntity, on_delete = models.CASCADE)
	connection_name = models.CharField('connection name', max_length = 100)
	connection_input = models.CharField('connection input', max_length = 100)
	connection_output = models.CharField('connection output', max_length = 100)