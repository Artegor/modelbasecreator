# from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render

from .models import StateEntity, Connection

def index(request):
	latest_state_entity = StateEntity.objects.order_by('entity_name')
	return render(request, 'creator_space/list.html', {'latest_state_entity':latest_state_entity})




