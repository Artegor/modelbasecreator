from django.apps import AppConfig


class CreatorSpaceConfig(AppConfig):
    name = 'creator_space'
